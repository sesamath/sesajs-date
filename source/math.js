import { formatDate } from './format'

const dayMs = 24 * 60 * 60 * 1000 // nb de ms dans une journée

/**
 * Retourne une nouvelle date, décalée de offset jours par rapport à date
 * @param {Date} date
 * @param {number} offset nb de jours à décaler (peut être négatif ou flottant)
 * @return {Date} La nouvelle date
 * @throws {TypeError} si date n'est pas une date ou offset pas un number
 */
export function addDays (date, offset) {
  if (!(date instanceof Date)) throw TypeError('date invalide')
  if (!Number.isFinite(offset)) throw TypeError('offset invalide')
  const ts = date.getTime()
  if (Number.isNaN(ts)) throw TypeError('date invalide')
  const date2 = new Date(ts + offset * dayMs)
  if (Number.isNaN(date2.getTime())) throw RangeError('offset trop grand : ' + offset)
  return date2
}

/**
 * Retourne une nouvelle date, décalée de offset secondes par rapport à date
 * @param {Date} date
 * @param {number} offset nb de jours à décaler (peut être négatif ou flottant)
 * @return {Date} La nouvelle date
 * @throws {TypeError} si date n'est pas une date ou offset pas un number
 */
export function addSecs (date, offset) {
  if (!(date instanceof Date)) throw TypeError('date invalide')
  if (!Number.isFinite(offset)) throw TypeError('offset invalide')
  const ts = date.getTime()
  if (Number.isNaN(ts)) throw TypeError('date invalide')
  const date2 = new Date(ts + offset * 1000)
  if (Number.isNaN(date2.getTime())) throw RangeError('offset trop grand : ' + offset)
  return date2
}

/**
 * Retourne date2 - date1 en jours (arrondi)
 * @param {Date} date1
 * @param {Date} date2
 * @params {Object} options
 * @params {boolean} [options.isFloat=false] passer true pour ne pas arrondir le résultat
 * @return {number} Le nb de jour entre ces dates (arrondi)
 * @throws {TypeError} si date1 ou date2 n'est pas une date
 */
export function diffDays (date1, date2, { isFloat = false } = {}) {
  if (!(date1 instanceof Date) || !(date2 instanceof Date)) throw TypeError('date invalide')
  const ts1 = date1.getTime()
  const ts2 = date2.getTime()
  if (Number.isNaN(ts1) || Number.isNaN(ts2)) throw TypeError('date invalide')
  const diff = (ts2 - ts1) / dayMs
  return isFloat ? diff : Math.round(diff)
}

/**
 * Retourne date2 - date1 en secondes (arrondi)
 * @param {Date} date1
 * @param {Date} date2
 * @return {number} Le nb de secondes entre ces dates (arrondi)
 * @throws {TypeError} si date1 ou date2 n'est pas une date
 */
export function diffSecs (date1, date2) {
  if (!(date1 instanceof Date) || !(date2 instanceof Date)) throw TypeError('date invalide')
  const ts1 = date1.getTime()
  const ts2 = date2.getTime()
  if (Number.isNaN(ts1) || Number.isNaN(ts2)) throw TypeError('date invalide')
  return Math.round((ts2 - ts1) / 1000)
}

/**
 * Retourne la date de la dernière ms du jour de date
 * @param {Date} [date] si non fourni ce sera la date courante
 * @returns {Date}
 */
export function endOfDay (date) {
  if (!(date instanceof Date)) date = new Date()
  return new Date(`${formatDate({ date })}T23:59:59.999`)
}

/**
 * Retourne true si date1 ≤ date2
 * @param {Date} date1
 * @param {Date} date2
 * @param {Object} [options]
 * @param {boolean} [options.strictMode=false] passer true pour une comparaison strict (isOrdered retournera alors false sur deux dates égales)
 * @return {boolean}
 */
export function isOrdered (date1, date2, { strictMode = false } = {}) {
  if (!(date1 instanceof Date) || !(date2 instanceof Date)) throw TypeError('date invalide')
  const ts1 = date1.getTime()
  const ts2 = date2.getTime()
  if (Number.isNaN(ts1) || Number.isNaN(ts2)) throw TypeError('date invalide')
  return strictMode
    ? ts1 < ts2
    : ts1 <= ts2
}

/**
 * Retourne la date de la première ms du jour de date
 * @param {Date} [date] si non fourni ce sera la date courante
 * @returns {Date}
 */
export function startOfDay (date) {
  if (!(date instanceof Date)) date = new Date()
  return new Date(`${formatDate({ date })}T00:00:00.000`)
}
