/**
 * Match une date au format dd/MM/YYYY (et capture les 3 morceaux)
 * @type {RegExp}
 */
export const dateFrRegexp = /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/([0-9]{4})$/

/**
 * Match une date au format YYYY-MM-dd (et capture les 3 morceaux)
 * @type {RegExp}
 */
export const dateIsoRegexp = /^([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/

/**
 * Match une date au format (-YY)?YYYY-MM-ddThh:mm:ss(.SSS)?Z (et capture les 7 morceaux)
 * @type {RegExp}
 */
export const dateJsonRegexp = /^((?:-[0-9]{2})?[0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])(?:\.([0-9]{3}))?Z/

/**
 * Retourne la date donnée au format YYYY-MM-dd ou dd/MM/YYYY ou (-?YY)?YYYY-MM-ddThh:mm:ss(.SSS)?Z
 * @param dateString
 * @return {Date}
 * @throws {TypeError} si dateString ne correspond pas à une date valide
 */
export function parse (dateString) {
  if (typeof dateString !== 'string') throw TypeError(`${dateString} n’est pas une date valide (string YYYY-MM-dd ou dd/MM/YYYY ou YYYY-MM-ddThh:mm:ss.SSSZ)`)
  if (dateIsoRegexp.test(dateString)) return parseDateIso(dateString)
  if (dateJsonRegexp.test(dateString)) return parseDateJson(dateString)
  if (dateFrRegexp.test(dateString)) return parseDateFr(dateString)
  throw TypeError(`${dateString} n’est pas une date valide (ni YYYY-MM-dd ni dd/MM/YYYY ni format json)`)
}

/**
 * Retourne la date dd/MM/YYYY
 * @param {string} dateString
 * @return {Date}
 * @throws {TypeError} si dateString ne correspond pas à une date valide
 */
export function parseDateFr (dateString) {
  if (typeof dateString !== 'string') throw TypeError(`Date invalide (pas une string dd/MM/YYYY) : ${typeof dateString}`)
  const chunks = dateFrRegexp.exec(dateString)
  if (chunks) {
    const [, d, m, y] = chunks
    const date = new Date(`${y}-${m}-${d}T12:00:00.000Z`)
    // Ça peut être Invalid Date, un objet Date dont le timestamp est NaN
    if (date.getTime()) return date
  }
  throw TypeError(`${dateString} n’est pas une date valide au format dd/MM/YYYY`)
}

/**
 * Retourne la date donnée au format YYYY-MM-dd
 * @param {string} dateString
 * @return {Date}
 * @throws {TypeError} si dateString ne correspond pas à une date valide
 */
export function parseDateIso (dateString) {
  if (dateIsoRegexp.test(dateString)) {
    const date = new Date(`${dateString}T12:00:00.000Z`)
    // Ça peut être Invalid Date, un objet Date dont le timestamp est NaN
    if (date.getTime()) return date
  }
  throw TypeError(`${dateString} n’est pas une date valide au format YYYY-MM-dd`)
}

/**
 * Retourne la date donnée au format json (-?YY)?YYYY-MM-ddThh:mm:ss(.SSS)?Z
 * @param {string} dateString
 * @return {Date}
 * @throws {TypeError} si dateString ne correspond pas à une date valide
 */
export function parseDateJson (dateString) {
  if (typeof dateString !== 'string') throw TypeError(`Date invalide (pas une string) : ${typeof dateString}`)
  if (dateJsonRegexp.test(dateString)) {
    const date = new Date(dateString)
    // Ça peut être Invalid Date, un objet Date dont le timestamp est NaN
    if (date.getTime()) return date
  }
  throw TypeError(`${dateString} n’est pas une date valide au format json YYYY-MM-ddThh:…`)
}

/**
 * Vérifie si dateString est une date plausible au format dd/MM/YYYY
 * (retourne true pour 30 février ou 31 avril, mais false pour 32 janvier), false si dateString n'est pas une string.
 * @param {string} dateString
 * @return {boolean}
 */
export const seemsDateFr = (dateString) => typeof dateString === 'string' && dateFrRegexp.test(dateString)

/**
 * Vérifie si dateString est une date plausible au format YYYY-MM-dd
 * (retourne true pour 30 février ou 31 avril, mais false pour 32 janvier), false si dateString n'est pas une string.
 * @param {string} dateString
 * @return {boolean}
 */
export const seemsDateIso = (dateString) => typeof dateString === 'string' && dateIsoRegexp.test(dateString)

/**
 * Vérifie si dateString est une date plausible au format (-?YY)?YYYY-MM-ddThh:mm:ss(.SSS)?Z
 * (retourne true pour 30 février ou 31 avril, mais false pour 32 janvier), false si dateString n'est pas une string.
 * @param {string} dateString
 * @return {boolean}
 */
export const seemsDateJson = (dateString) => typeof dateString === 'string' && dateJsonRegexp.test(dateString)
