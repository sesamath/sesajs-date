/**
 * Module pour manipuler date & heures (parse / format / addition / soustraction)
 */
// Cf https://medium.com/swlh/best-moment-js-alternatives-5dfa6861a1eb pour une comparaison des modules npm existants fin 2020
// Cf https://date-fns.org/docs/Getting-Started pour date-fns

// On réexporte toutes nos fonctions issues des ≠ fichiers
export { formatDate, formatDateTime, formatTime } from './format'
export { parse, parseDateFr, parseDateJson, parseDateIso, seemsDateFr, seemsDateIso, seemsDateJson } from './parse'
export { addDays, diffDays, addSecs, diffSecs, endOfDay, startOfDay, isOrdered } from './math'
