/**
 * Module pour le formatage de date & heure, sans dépendance externe
 */

// ////////////////////////////
// fcts privées
// ////////////////////////////

/**
 * Retourne un nb sur deux chiffres en string (0 initial ajouté si besoin)
 * @private
 * @param {number} nb
 * @return {string} nb avec 2 chiffres
 */
const f2 = (nb) => (nb < 10 ? '0' : '') + nb
/**
 * Retourne un nb sur trois chiffres en string (0 initiaux ajoutés si besoin)
 * @private
 * @param {number} nb
 * @return {string} nb avec 3 chiffres
 */
const f3 = (nb) => (nb < 10 ? '00' : (nb < 100 ? '0' : '')) + nb

/**
 * Retourne [annee, mois, jour] en string
 * @private
 * @param {Date} [date]
 * @return {string[]}
 */
const getDateChunks = (date) => {
  if (!date) date = new Date()
  const year = date.getFullYear()
  const monthNb = date.getMonth() + 1
  const dayNb = date.getDate()
  return [String(year), f2(monthNb), f2(dayNb)]
}

/**
 * Retourne [heure, minute, seconde, ms] en string
 * @private
 * @param {Date} [date]
 * @return {string[]}
 */
const getTimeChunks = (date) => {
  if (!date) date = new Date()
  const h = date.getHours()
  const m = date.getMinutes()
  const s = date.getSeconds()
  const ms = date.getMilliseconds()
  return [f2(h), f2(m), f2(s), f3(ms)]
}

/**
 * Retourne la date et les options après parsing de l'argument passé
 * @private
 * @param {Object} options
 * @param {Date} [options.date] La date à utiliser (plutôt que la date d'appel de la fct)
 * @param {number} [options.ts] Le timestamp à utiliser (timestamp js en ms, pas timestamp unix en s)
 * @return {{date: Date, shortYear: boolean, fr: boolean, withoutMs: boolean, withoutSec: boolean }}
 */
const parseOptions = (options) => {
  const shortYear = options?.shortYear === true
  const fr = options?.fr === true
  const withoutMs = options?.withoutMs === true
  const withoutSec = options?.withoutSec === true
  let date
  if (typeof options === 'number') {
    date = new Date(options)
  } else if (options instanceof Date) {
    date = options
  } else if (options == null) {
    date = new Date()
  } else if (typeof options === 'object') {
    if (options.date instanceof Date) date = options.date
    else if (typeof options.ts === 'number') date = new Date(options.ts)
  }
  if (Number.isNaN(date?.valueOf())) {
    let optStr
    try {
      optStr = JSON.stringify(options)
    } catch (error) {
      optStr = error.message
    }
    throw Error(`Date invalide : ${optStr}`)
  }
  return { date, shortYear, fr, withoutMs, withoutSec }
}

// ////////////////////////////
// fcts exportées
// ////////////////////////////

/**
 * Retourne la date formatée YYYY-MM-dd par défaut
 * @param {Object|number|Date} [options] La date (ou son timestamp) à utiliser, ou un objet d'options. Si non fourni ce sera la date du moment
 * @param {Date} [options.date] La date à utiliser (plutôt que la date d'appel de la fct)
 * @param {boolean} [options.fr=false] passer true pour récupérer dd/MM/YYYY plutôt que YYYY-MM-dd
 * @param {boolean} [options.shortYear=false] passer true pour récupérer YY plutôt que YYYY
 * @param {number} [options.ts] Le timestamp à utiliser (timestamp js en ms, pas timestamp unix en s)
 * @return {string}
 */
export function formatDate (options) {
  const { date, shortYear, fr } = parseOptions(options)
  const [y, month, day] = getDateChunks(date)
  const year = shortYear ? y.substring(2, 4) : y
  return fr ? `${day}/${month}/${year}` : `${year}-${month}-${day}`
}

/**
 * Retourne la date formatée YYYY-MM-dd HH:mm:ss.SSS
 * @param {Object} options
 * @param {Date} [options.date] La date à utiliser (plutôt que la date courante)
 * @param {boolean} [options.fr=false] passer true pour récupérer dd/MM/YYYY plutôt que YYYY-MM-dd
 * @param {boolean} [options.shortYear=false] passer true pour récupérer YY plutôt que YYYY
 * @param {number} [options.ts] Le timestamp à utiliser (timestamp js en ms, pas timestamp unix en s)
 * @param {boolean} [options.withoutMs=false] passer true pour ne pas avoir les ms dans l'heure
 * @param {boolean} [options.withoutSec=false] passer true pour avoir l'heure au format HH:mm (sans seconde ni ms)
 * @return {string}
 */
export function formatDateTime (options) {
  // on impose la même date pour les deux fonctions, au cas où on changerait de jour entre leur appel…
  // (ce serait pas de bol mais ça peut arriver d'appeler cette fct pile à minuit -1ms)
  const opts = parseOptions(options)
  return `${formatDate(opts)} ${formatTime(opts)}`
}

/**
 * Retourne l'heure, formatée par défaut en HH:mm:ss.SSS
 * @param {Object} options
 * @param {Date} [options.date] La date à utiliser (plutôt que la date d'appel de la fct)
 * @param {number} [options.ts] Le timestamp à utiliser (timestamp js en ms, pas timestamp unix en s)
 * @param {boolean} [options.withoutMs=false] passer true pour ne pas avoir les ms dans l'heure
 * @param {boolean} [options.withoutSec=false] passer true pour avoir l'heure au format HH:mm (sans seconde ni ms)
 * @return {string}
 */
export function formatTime (options) {
  const { date, withoutMs, withoutSec } = parseOptions(options)
  const [h, m, s, ms] = getTimeChunks(date)
  if (withoutSec) return `${h}:${m}`
  if (withoutMs) return `${h}:${m}:${s}`
  return `${h}:${m}:${s}.${ms}`
}
