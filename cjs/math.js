'use strict';

/**
 * Module pour le formatage de date & heure, sans dépendance externe
 */

// ////////////////////////////
// fcts privées
// ////////////////////////////

/**
 * Retourne un nb sur deux chiffres en string (0 initial ajouté si besoin)
 * @private
 * @param {number} nb
 * @return {string} nb avec 2 chiffres
 */
const f2 = (nb) => (nb < 10 ? '0' : '') + nb;

/**
 * Retourne [annee, mois, jour] en string
 * @private
 * @param {Date} [date]
 * @return {string[]}
 */
const getDateChunks = (date) => {
  if (!date) date = new Date();
  const year = date.getFullYear();
  const monthNb = date.getMonth() + 1;
  const dayNb = date.getDate();
  return [String(year), f2(monthNb), f2(dayNb)]
};

/**
 * Retourne la date et les options après parsing de l'argument passé
 * @private
 * @param {Object} options
 * @param {Date} [options.date] La date à utiliser (plutôt que la date d'appel de la fct)
 * @param {number} [options.ts] Le timestamp à utiliser (timestamp js en ms, pas timestamp unix en s)
 * @return {{date: Date, shortYear: boolean, fr: boolean, withoutMs: boolean, withoutSec: boolean }}
 */
const parseOptions = (options) => {
  const shortYear = options?.shortYear === true;
  const fr = options?.fr === true;
  const withoutMs = options?.withoutMs === true;
  const withoutSec = options?.withoutSec === true;
  let date;
  if (typeof options === 'number') {
    date = new Date(options);
  } else if (options instanceof Date) {
    date = options;
  } else if (options == null) {
    date = new Date();
  } else if (typeof options === 'object') {
    if (options.date instanceof Date) date = options.date;
    else if (typeof options.ts === 'number') date = new Date(options.ts);
  }
  if (Number.isNaN(date?.valueOf())) {
    let optStr;
    try {
      optStr = JSON.stringify(options);
    } catch (error) {
      optStr = error.message;
    }
    throw Error(`Date invalide : ${optStr}`)
  }
  return { date, shortYear, fr, withoutMs, withoutSec }
};

// ////////////////////////////
// fcts exportées
// ////////////////////////////

/**
 * Retourne la date formatée YYYY-MM-dd par défaut
 * @param {Object|number|Date} [options] La date (ou son timestamp) à utiliser, ou un objet d'options. Si non fourni ce sera la date du moment
 * @param {Date} [options.date] La date à utiliser (plutôt que la date d'appel de la fct)
 * @param {boolean} [options.fr=false] passer true pour récupérer dd/MM/YYYY plutôt que YYYY-MM-dd
 * @param {boolean} [options.shortYear=false] passer true pour récupérer YY plutôt que YYYY
 * @param {number} [options.ts] Le timestamp à utiliser (timestamp js en ms, pas timestamp unix en s)
 * @return {string}
 */
function formatDate (options) {
  const { date, shortYear, fr } = parseOptions(options);
  const [y, month, day] = getDateChunks(date);
  const year = shortYear ? y.substring(2, 4) : y;
  return fr ? `${day}/${month}/${year}` : `${year}-${month}-${day}`
}

const dayMs = 24 * 60 * 60 * 1000; // nb de ms dans une journée

/**
 * Retourne une nouvelle date, décalée de offset jours par rapport à date
 * @param {Date} date
 * @param {number} offset nb de jours à décaler (peut être négatif ou flottant)
 * @return {Date} La nouvelle date
 * @throws {TypeError} si date n'est pas une date ou offset pas un number
 */
function addDays (date, offset) {
  if (!(date instanceof Date)) throw TypeError('date invalide')
  if (!Number.isFinite(offset)) throw TypeError('offset invalide')
  const ts = date.getTime();
  if (Number.isNaN(ts)) throw TypeError('date invalide')
  const date2 = new Date(ts + offset * dayMs);
  if (Number.isNaN(date2.getTime())) throw RangeError('offset trop grand : ' + offset)
  return date2
}

/**
 * Retourne une nouvelle date, décalée de offset secondes par rapport à date
 * @param {Date} date
 * @param {number} offset nb de jours à décaler (peut être négatif ou flottant)
 * @return {Date} La nouvelle date
 * @throws {TypeError} si date n'est pas une date ou offset pas un number
 */
function addSecs (date, offset) {
  if (!(date instanceof Date)) throw TypeError('date invalide')
  if (!Number.isFinite(offset)) throw TypeError('offset invalide')
  const ts = date.getTime();
  if (Number.isNaN(ts)) throw TypeError('date invalide')
  const date2 = new Date(ts + offset * 1000);
  if (Number.isNaN(date2.getTime())) throw RangeError('offset trop grand : ' + offset)
  return date2
}

/**
 * Retourne date2 - date1 en jours (arrondi)
 * @param {Date} date1
 * @param {Date} date2
 * @params {Object} options
 * @params {boolean} [options.isFloat=false] passer true pour ne pas arrondir le résultat
 * @return {number} Le nb de jour entre ces dates (arrondi)
 * @throws {TypeError} si date1 ou date2 n'est pas une date
 */
function diffDays (date1, date2, { isFloat = false } = {}) {
  if (!(date1 instanceof Date) || !(date2 instanceof Date)) throw TypeError('date invalide')
  const ts1 = date1.getTime();
  const ts2 = date2.getTime();
  if (Number.isNaN(ts1) || Number.isNaN(ts2)) throw TypeError('date invalide')
  const diff = (ts2 - ts1) / dayMs;
  return isFloat ? diff : Math.round(diff)
}

/**
 * Retourne date2 - date1 en secondes (arrondi)
 * @param {Date} date1
 * @param {Date} date2
 * @return {number} Le nb de secondes entre ces dates (arrondi)
 * @throws {TypeError} si date1 ou date2 n'est pas une date
 */
function diffSecs (date1, date2) {
  if (!(date1 instanceof Date) || !(date2 instanceof Date)) throw TypeError('date invalide')
  const ts1 = date1.getTime();
  const ts2 = date2.getTime();
  if (Number.isNaN(ts1) || Number.isNaN(ts2)) throw TypeError('date invalide')
  return Math.round((ts2 - ts1) / 1000)
}

/**
 * Retourne la date de la dernière ms du jour de date
 * @param {Date} [date] si non fourni ce sera la date courante
 * @returns {Date}
 */
function endOfDay (date) {
  if (!(date instanceof Date)) date = new Date();
  return new Date(`${formatDate({ date })}T23:59:59.999`)
}

/**
 * Retourne true si date1 ≤ date2
 * @param {Date} date1
 * @param {Date} date2
 * @param {Object} [options]
 * @param {boolean} [options.strictMode=false] passer true pour une comparaison strict (isOrdered retournera alors false sur deux dates égales)
 * @return {boolean}
 */
function isOrdered (date1, date2, { strictMode = false } = {}) {
  if (!(date1 instanceof Date) || !(date2 instanceof Date)) throw TypeError('date invalide')
  const ts1 = date1.getTime();
  const ts2 = date2.getTime();
  if (Number.isNaN(ts1) || Number.isNaN(ts2)) throw TypeError('date invalide')
  return strictMode
    ? ts1 < ts2
    : ts1 <= ts2
}

/**
 * Retourne la date de la première ms du jour de date
 * @param {Date} [date] si non fourni ce sera la date courante
 * @returns {Date}
 */
function startOfDay (date) {
  if (!(date instanceof Date)) date = new Date();
  return new Date(`${formatDate({ date })}T00:00:00.000`)
}

exports.addDays = addDays;
exports.addSecs = addSecs;
exports.diffDays = diffDays;
exports.diffSecs = diffSecs;
exports.endOfDay = endOfDay;
exports.isOrdered = isOrdered;
exports.startOfDay = startOfDay;
