import { describe, expect, it } from 'vitest'
import {
  dateFrRegexp, dateIsoRegexp, dateJsonRegexp,
  parse,
  parseDateFr,
  parseDateIso,
  parseDateJson,
  seemsDateFr,
  seemsDateIso,
  seemsDateJson
} from '../source/parse'

describe('parseDateFr retourne une date (à midi UTC)', () => {
  it('si elle est correctement formatée', () => {
    const date = parseDateFr('01/02/2003')
    expect(date).to.be.a('Date')
    expect(date.toJSON()).to.match(/^2003-02-01T12:00:00.000Z$/)
  })
  it('plante sinon', () => {
    ;[
      '2003-01-02',
      '32/01/2003',
      '24/13/2003',
      '24/12/03',
      '24/12/2003 ',
      ' 24/12/2003',
      'foo/bar/baz',
      'foo'
    ].forEach((date) => {
      const fn = parseDateFr.bind(null, date)
      expect(fn).to.throw(TypeError, date, `Pb avec ${date}`)
    })
  })
})
describe('parseDateIso retourne une date (à midi UTC)', () => {
  it('si elle est correctement formatée YYYY-MM-dd', () => {
    const date = parseDateIso('2003-02-01')
    expect(date).to.be.a('Date')
    expect(date.toJSON()).to.match(/^2003-02-01T12:00:00.000Z$/)
  })
  it('plante sinon', () => {
    ;[
      '01/02/2003',
      '2003-01-32',
      '2003-13-24',
      '03-12-24',
      '2003-12-24 ',
      ' 2003-12-24',
      'foo-bar-baz',
      'foo'
    ].forEach((date) => {
      const fn = parseDateIso.bind(null, date)
      expect(fn).to.throw(TypeError, date, `Pb avec ${date}`)
    })
  })
})
describe('parseDateJon retourne la date', () => {
  it('si elle est correctement formatée YYYY-MM-ddThh:mm:ss.SSSZ', () => {
    ;[
      '2003-02-01T04:05:06.007Z',
      '9999-02-01T04:05:06.007Z'
    ].forEach((dateStr) => {
      const date = parseDateJson(dateStr)
      expect(date).to.be.a('Date')
      expect(date.toJSON()).to.equals(dateStr)
    })
  })
  it('ou sans les ms (YYYY-MM-ddThh:mm:ssZ)', () => {
    ;[
      '2003-02-01T04:05:06Z',
      '-123456-02-01T04:05:06Z',
      '9999-02-01T04:05:06Z'
    ].forEach((dateStr) => {
      const date = parseDateJson(dateStr)
      expect(date).to.be.a('Date')
      expect(date.toJSON()).to.equals(dateStr.replace('Z', '.000Z'))
    })
  })
  it('plante sinon (y compris avec TZ, ou +00 plutôt que Z, ou heure invalide)', () => {
    ;[
      '01/02/03',
      '2003-02-01',
      '03-02-01',
      'foo/bar/baz',
      '2003-02-01T04:05:06.007',
      '2003-02-01T04:05:06.007+01',
      '2003-02-01T04:05:06+01',
      '2003-02-01T04:05:06.007+00',
      '2003-02-01T04:05:06+00',
      '2003-02-01T04:05:06.007+0000',
      '2003-02-01T04:05:06+0000',
      '2003-02-01T04:05:66.007Z',
      '2003-02-01T04:65:06.007Z',
      '2003-02-01T24:05:06.007Z',
      '-002003-02-01T24:05:06.007Z',
      '001234-02-01T24:05:06.007Z',
      '123456-02-01T24:05:06.007Z'
    ].forEach((date) => {
      const fn = parseDateJson.bind(null, date)
      expect(fn).to.throw(TypeError, date, `Pb avec ${date}`)
    })
  })
})
describe('parse retourne une date', () => {
  it('si elle est correctement formatée avec un des 3 formats précédents', () => {
    // parse utilise la regexp testée plus bas pour appelée parseJson testée précédemment, pas la peine de tester plein de cas
    const dateJson = '2003-02-01T04:05:06.007Z'
    let date = parse(dateJson)
    expect(date).to.be.a('Date')
    expect(date.toJSON()).to.equals(dateJson)
    // idem sans ms
    expect(parse('2003-02-01T04:05:06Z').toJSON()).to.equals('2003-02-01T04:05:06.000Z')
    ;['2003-02-01', '01/02/2003'].forEach((dateStr) => {
      date = parse(dateStr)
      expect(date).to.be.a('Date')
      expect(date.toJSON()).to.equals('2003-02-01T12:00:00.000Z')
    })
  })
  it('plante sinon', () => {
    ;[
      '01/02/03',
      '32/01/2003',
      '24/13/2003',
      '24/12/03',
      '24/12/2003 ',
      ' 24/12/2003',
      'foo/bar/baz',
      'foo',
      '03-02-01',
      '2003-01-32',
      '2003-13-24',
      '03-12-24',
      '2003-12-24 ',
      ' 2003-12-24',
      'foo-bar-baz',
      '2003-02-01T04:05:06.007',
      '2003-02-01T04:05:06.007+01',
      '2003-02-01T04:05:06+01',
      '2003-02-01T04:05:06.007+00',
      '2003-02-01T04:05:06+00',
      '2003-02-01T04:05:06.007+0000',
      '2003-02-01T04:05:06+0000'
    ].forEach((date) => {
      const fn = parse.bind(null, date)
      expect(fn).to.throw(TypeError, date, `Pb avec ${date}`)
    })
  })
})
describe('seemsDateFr', () => {
  it('retourne true sur une date plausible (y compris 30 février ou 31 avril)', () => {
    ;['01/02/2001', '01/02/0001', '30/02/2003', '31/04/2042'].forEach(date => {
      expect(seemsDateFr(date)).to.be.true
    })
  })
  it('et false sur le reste (32 janvier ou 01/13/xxxx ou 00/mm/yyyy)', () => {
    ;['foo', '32/01/2042', '46/02/1234', '00/01/1234', '99/99/9999', '1234-01-02'].forEach(date => {
      expect(seemsDateFr(date)).to.be.false
    })
  })
})
describe('seemsDateIso', () => {
  it('retourne true sur une date plausible', () => {
    ;['2001-02-01', '0001-09-08', '1234-02-30', '1234-04-31'].forEach(date => {
      expect(seemsDateIso(date)).to.be.true
    })
  })
  it('et false sur le reste', () => {
    ;['01/02/2001', '1234-01-32', '1234', '2042-04-00', '1234-99-01', '9999-99-99'].forEach(date => {
      expect(seemsDateIso(date)).to.be.false
    })
  })
})
describe('seemsDateJson', () => {
  it('retourne true sur une date plausible', () => {
    ;['2003-02-01T04:05:06.007Z', '2003-04-31T00:00:00.000Z', '-002003-02-01T04:05:06.007Z', '9999-02-01T04:05:06.007Z'].forEach(date => {
      expect(seemsDateJson(date)).to.be.true
    })
  })
  it('et false sur le reste', () => {
    ;[
      '2003-02-01T24:05:06.007Z',
      '2003-02-01T04:65:06.007Z',
      '2003-02-01T04:05:66.007Z',
      '123456-02-01T04:05:06.007Z'
    ].forEach(date => {
      expect(seemsDateJson(date)).to.be.false
    })
  })
})
describe('dateFrRegexp', () => {
  it('match sur une date plausible (et capture mois et années)', () => {
    ;['01/02/2003', '31/02/2003'].forEach(date => {
      expect(date).to.match(dateFrRegexp)
      const expectedChunks = date.split('/')
      const chunks = dateFrRegexp.exec(date)
      chunks.slice(1, 4).forEach((chunk, i) => {
        expect(chunk).to.equals(expectedChunks[i])
      })
    })
  })
  it('match pas sur le reste', () => {
    ;['01/13/2001', '1234-01-32', '1234', '2042-04-00', '99/01/1234', '01/01/01', '00/01/1234', '01/00/1234'].forEach(date => {
      expect(date).not.to.match(dateFrRegexp)
    })
  })
})
describe('dateIsoRegexp', () => {
  it('match sur une date plausible (et capture mois et années)', () => {
    ;['2003-02-01', '2003-02-31'].forEach(date => {
      expect(date).to.match(dateIsoRegexp)
      const expectedChunks = date.split('-')
      const chunks = dateIsoRegexp.exec(date)
      chunks.slice(1, 4).forEach((chunk, i) => {
        expect(chunk).to.equals(expectedChunks[i])
      })
    })
  })
  it('match pas sur le reste', () => {
    ;['01/02/2001', '1234-13-02', '1234', '2042-04-00', '1234-01-32', '2003-02-01T04:05:06.007Z'].forEach(date => {
      expect(date).not.to.match(dateIsoRegexp)
    })
  })
})
describe('dateJsonRegexp', () => {
  it('match sur une date plausible (et capture mois et années)', () => {
    ;['2003-02-01T04:05:06.007Z', '2003-02-01T04:05:06Z'].forEach((date, i) => {
      expect(date).to.match(dateJsonRegexp)
      const expectedChunks = [date, '2003', '02', '01', '04', '05', '06', i ? undefined : '007']
      const chunks = dateJsonRegexp.exec(date)
      chunks.forEach((chunk, i) => {
        expect(chunk).to.equals(expectedChunks[i])
      })
    })
    // on teste aussi un négatif
    const dateBc = '-123456-02-01T04:05:06.007Z'
    const expectedChunks = [dateBc, '-123456', '02', '01', '04', '05', '06', '007']
    dateJsonRegexp.exec(dateBc).forEach((chunk, i) => {
      expect(chunk).to.equals(expectedChunks[i])
    })
  })
  it('match pas sur le reste', () => {
    // déjà testé dans seemsDateJson
    ;['01/02/2001', '1234-13-02', '1234', '2042-04-00', '1234-01-32', '2003-02-01T04:65:06.007Z'].forEach(date => {
      expect(date).not.to.match(dateJsonRegexp)
    })
  })
})
