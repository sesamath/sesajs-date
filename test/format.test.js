import { describe, expect, it } from 'vitest'

import { formatDate, formatDateTime, formatTime } from '../source/format'

const ts = 1616171633248
const date = new Date(ts) // 2021-03-19T16:33:53.248Z => 2021-03-19 17:33:53.248 UTC+1
const ts2 = 981173168007
const date2 = new Date(ts2) // 2001-02-03 04:06:08.007Z

describe('formatDate formate une date (date courante ou imposée avec date ou imposée avec ts)', () => {
  it('format YYYY-MM-dd par défaut', () => {
    const now = formatDate()
    expect(now).to.match(/^[0-9]{4}-(0[1-9]|1[012])-[0-3][0-9]$/)
    expect(formatDate({ date })).to.equals('2021-03-19')
    expect(formatDate({ ts })).to.equals('2021-03-19')
    expect(formatDate({ date: date2 })).to.equals('2001-02-03')
    expect(formatDate({ ts: ts2 })).to.equals('2001-02-03')
  })
  it('format YY-MM-dd avec l’option shortYear', () => {
    const now = formatDate({ shortYear: true })
    expect(now).to.match(/^[0-9]{2}-(0[1-9]|1[012])-[0-3][0-9]$/)
    expect(formatDate({ date, shortYear: true })).to.equals('21-03-19')
    expect(formatDate({ ts, shortYear: true })).to.equals('21-03-19')
    expect(formatDate({ date: date2, shortYear: true })).to.equals('01-02-03')
    expect(formatDate({ ts: ts2, shortYear: true })).to.equals('01-02-03')
  })
  it('format dd/MM/YYYY avec l’option fr', () => {
    const now = formatDate({ fr: true })
    expect(now).to.match(/^[0-3][0-9]\/(0[1-9]|1[012])\/[0-9]{4}$/)
    expect(formatDate({ date, fr: true })).to.equals('19/03/2021')
    expect(formatDate({ ts, fr: true })).to.equals('19/03/2021')
    expect(formatDate({ date: date2, fr: true })).to.equals('03/02/2001')
    expect(formatDate({ ts: ts2, fr: true })).to.equals('03/02/2001')
  })
  it('format dd/MM/YY avec l’option fr+shortYear', () => {
    const now = formatDate({ shortYear: true, fr: true })
    expect(now).to.match(/^[0-3][0-9]\/(0[1-9]|1[012])\/[0-9]{2}$/)
    expect(formatDate({ date, fr: true, shortYear: true })).to.equals('19/03/21')
    expect(formatDate({ ts, fr: true, shortYear: true })).to.equals('19/03/21')
    expect(formatDate({ date: date2, fr: true, shortYear: true })).to.equals('03/02/01')
    expect(formatDate({ ts: ts2, fr: true, shortYear: true })).to.equals('03/02/01')
  })
})
describe('formatTime formate l’heure (courante ou imposée)', () => {
  it('format hh:mm:ss.SSS par défaut', () => {
    const now = formatTime()
    expect(now).to.match(/^[012][0-9]:[0-5][0-9]:[0-5][0-9]\.[0-9]{3}$/)
    // on ne compare pas un autre new Date() car il peut y avoir 1ms d'écart
    expect(formatTime({ date })).to.equals('17:33:53.248')
    expect(formatTime({ ts })).to.equals('17:33:53.248')
    expect(formatTime({ date: date2 })).to.equals('05:06:08.007')
    expect(formatTime({ ts: ts2 })).to.equals('05:06:08.007')
  })
  it('format hh:mm:ss avec l’option withoutMs', () => {
    const now = formatTime({ withoutMs: true })
    expect(now).to.match(/^[012][0-9]:[0-5][0-9]:[0-5][0-9]$/)
    expect(formatTime({ date, withoutMs: true })).to.equals('17:33:53')
    expect(formatTime({ ts, withoutMs: true })).to.equals('17:33:53')
    expect(formatTime({ date: date2, withoutMs: true })).to.equals('05:06:08')
    expect(formatTime({ ts: ts2, withoutMs: true })).to.equals('05:06:08')
  })
  it('format hh:mm avec l’option withoutSec (indépendamment de withoutMs)', () => {
    const now = formatTime({ withoutSec: true })
    expect(now).to.match(/^[012][0-9]:[0-9]{2}$/)
    const now2 = formatTime({ withoutSec: true, withoutMs: true })
    expect(now2).to.match(/^[012][0-9]:[0-9]{2}$/)
    expect(formatTime({ date, withoutSec: true })).to.equals('17:33')
    expect(formatTime({ date, withoutSec: true, withoutMs: true })).to.equals('17:33')
    expect(formatTime({ ts, withoutSec: true })).to.equals('17:33')
    expect(formatTime({ date: date2, withoutSec: true })).to.equals('05:06')
    expect(formatTime({ ts: ts2, withoutSec: true })).to.equals('05:06')
  })
})

describe('formatDateTime formate date et heure', () => {
  it('format YYYY-MM-dd hh:mm:ss.SSS par défaut', () => {
    const now = formatDateTime()
    expect(now).to.match(/^[0-9]{4}-(0[1-9]|1[012])-[0-3][0-9] [012][0-9]:[0-5][0-9]:[0-5][0-9]\.[0-9]{3}$/)
    expect(formatDateTime({ date })).to.equals('2021-03-19 17:33:53.248')
    expect(formatDateTime({ ts })).to.equals('2021-03-19 17:33:53.248')
    expect(formatDateTime({ date: date2 })).to.equals('2001-02-03 05:06:08.007')
    expect(formatDateTime({ ts: ts2 })).to.equals('2001-02-03 05:06:08.007')
  })
  it('format YY-MM-dd hh:mm:ss.SSS avec shortYear', () => {
    const now = formatDateTime({ shortYear: true })
    expect(now).to.match(/^[0-9]{2}-(0[1-9]|1[012])-[0-3][0-9] [012][0-9]:[0-5][0-9]:[0-5][0-9]\.[0-9]{3}$/)
    expect(formatDateTime({ date, shortYear: true })).to.equals('21-03-19 17:33:53.248')
    expect(formatDateTime({ ts, shortYear: true })).to.equals('21-03-19 17:33:53.248')
    expect(formatDateTime({ date: date2, shortYear: true })).to.equals('01-02-03 05:06:08.007')
    expect(formatDateTime({ ts: ts2, shortYear: true })).to.equals('01-02-03 05:06:08.007')
  })
  it('format dd/MM/YYYY hh:mm:ss.SSS avec fr', () => {
    const now = formatDateTime({ fr: true })
    expect(now).to.match(/^[0-9]{2}\/(0[1-9]|1[012])\/[0-9]{4} [012][0-9]:[0-5][0-9]:[0-5][0-9]\.[0-9]{3}$/)
    expect(formatDateTime({ date, fr: true })).to.equals('19/03/2021 17:33:53.248')
    expect(formatDateTime({ ts, fr: true })).to.equals('19/03/2021 17:33:53.248')
    expect(formatDateTime({ date: date2, fr: true })).to.equals('03/02/2001 05:06:08.007')
    expect(formatDateTime({ ts: ts2, fr: true })).to.equals('03/02/2001 05:06:08.007')
  })
  it('format YYYY-MM-dd hh:mm:ss avec withoutMs', () => {
    const now = formatDateTime({ withoutMs: true })
    expect(now).to.match(/^[0-9]{4}-(0[1-9]|1[012])-[0-3][0-9] [012][0-9]:[0-5][0-9]:[0-5][0-9]$/)
    expect(formatDateTime({ date, withoutMs: true })).to.equals('2021-03-19 17:33:53')
    expect(formatDateTime({ ts, withoutMs: true })).to.equals('2021-03-19 17:33:53')
    expect(formatDateTime({ date: date2, withoutMs: true })).to.equals('2001-02-03 05:06:08')
    expect(formatDateTime({ ts: ts2, withoutMs: true })).to.equals('2001-02-03 05:06:08')
  })
  it('format YY-MM-dd hh:mm:ss avec shortYear+withoutMs', () => {
    const opts = { shortYear: true, withoutMs: true }
    const now = formatDateTime(opts)
    expect(now).to.match(/^[0-9]{2}-(0[1-9]|1[012])-[0-3][0-9] [012][0-9]:[0-5][0-9]:[0-5][0-9]$/)
    expect(formatDateTime({ ...opts, date })).to.equals('21-03-19 17:33:53')
    expect(formatDateTime({ ...opts, ts })).to.equals('21-03-19 17:33:53')
    expect(formatDateTime({ ...opts, date: date2 })).to.equals('01-02-03 05:06:08')
    expect(formatDateTime({ ...opts, ts: ts2 })).to.equals('01-02-03 05:06:08')
  })
  it('format dd/MM/YYYY hh:mm:ss avec fr+withoutMs', () => {
    const opts = { fr: true, withoutMs: true }
    const now = formatDateTime(opts)
    expect(now).to.match(/^[0-3][0-9]\/(0[1-9]|1[012])\/[0-9]{4} [012][0-9]:[0-5][0-9]:[0-5][0-9]$/)
    expect(formatDateTime({ ...opts, date })).to.equals('19/03/2021 17:33:53')
    expect(formatDateTime({ ...opts, ts })).to.equals('19/03/2021 17:33:53')
    expect(formatDateTime({ ...opts, date: date2 })).to.equals('03/02/2001 05:06:08')
    expect(formatDateTime({ ...opts, ts: ts2 })).to.equals('03/02/2001 05:06:08')
  })
  it('format dd/MM/YYYY hh:mm:ss avec fr+withoutSec', () => {
    const opts = { fr: true, withoutSec: true }
    const now = formatDateTime(opts)
    expect(now).to.match(/^[0-3][0-9]\/(0[1-9]|1[012])\/[0-9]{4} [012][0-9]:[0-5][0-9]$/)
    expect(formatDateTime({ ...opts, date })).to.equals('19/03/2021 17:33')
    expect(formatDateTime({ ...opts, ts })).to.equals('19/03/2021 17:33')
    expect(formatDateTime({ ...opts, date: date2 })).to.equals('03/02/2001 05:06')
    expect(formatDateTime({ ...opts, ts: ts2 })).to.equals('03/02/2001 05:06')
  })
})
