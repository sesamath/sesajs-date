import { describe, expect, it } from 'vitest'
import { formatDate, formatTime } from '../source/format'
import { addDays, addSecs, diffDays, diffSecs, endOfDay, isOrdered, startOfDay } from '../source/math'

const hourMs = 3600 * 1000
const dayMs = 24 * hourMs

const ts = 1616171633248
const date = new Date(ts) // 2021-03-19T16:33:53.248Z => 2021-03-19 17:33:53.248 UTC+1
const offsetHours = {
  p11h: 11,
  p13h: 13,
  p35h: 35,
  p37h: 37,
  p3dm2h: 3 * 24 - 2
}
const offsetMs = {
  p3s: 3000,
  p5s: 4501,
  p59s: 59499,
  p3600s: 3600400
}

/** collection de dates */
const dates = {}
for (const [name, hours] of Object.entries(offsetHours)) {
  dates[name] = new Date(ts + hours * hourMs)
}
for (const [name, ms] of Object.entries(offsetMs)) {
  dates[name] = new Date(ts + ms)
}

/** équivalent des offsetHours en jours entiers */
const offsetDays = {}
/** équivalent des offsetHours en jours flottants */
const offsetDaysFloat = {}
for (const [name, hours] of Object.entries(offsetHours)) {
  offsetDays[name] = Math.round(hours / 24)
  offsetDaysFloat[name] = hours / 24
}
/** équivalent des offsetMs en secondes entieres */
const offsetS = {}
for (const [name, ms] of Object.entries(offsetMs)) {
  offsetS[name] = Math.round(ms / 1000)
}

const badDates = [undefined, null, 0, false, true, new Date('foo')]
const badNumbers = [undefined, null, false, true, '', 'foo', new Date(), /foo/, Number.NaN, Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY]

describe('addDays', () => {
  it('Ajoute ou retire des jours', () => {
    for (const days of [-12345, -42, -2.3, -1, 0, 1e-6, 1, 2, Math.PI, 42, 12345.678901, 1e7]) {
      const date2 = addDays(date, days)
      // lors de la création d'une date la partie décimale du timestamp en ms est virée (pas d'arrondi)
      expect(date2.getTime()).to.equals(Math.floor(ts + days * dayMs), `Pb avec ${days}j qui donne ${date2}`)
    }
  })
  it('plante si on lui file une date invalide ou un nb de jours invalide (NaN ou infini)', () => {
    for (const badDate of badDates) {
      const fn = addDays.bind(null, badDate, 1)
      expect(fn).to.throw(TypeError, 'date invalide', `pb avec ${badDate}`)
    }
    const badNumbers = [undefined, null, false, true, '', 'foo', new Date(), /foo/, Number.NaN, Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY]
    for (const badNumber of badNumbers) {
      const fn = addDays.bind(null, date, badNumber)
      expect(fn).to.throw(TypeError, 'invalide', `pb avec ${badNumber}`)
    }
  })
  it('plante si on lui file nb de jours trop grand', () => {
    for (const tooBig of [-1e12, 1e12]) {
      const fn = addDays.bind(null, date, tooBig)
      expect(fn).to.throw(RangeError, 'offset trop grand', `pb avec ${tooBig}`)
    }
  })
})

describe('addSecs', () => {
  it('Ajoute ou retire des secondes', () => {
    for (const secs of [-12345, -42, -2.3, -1, 0, Number.EPSILON, 0.001, 1, 2, Math.PI, 42, 12345.678901, 1e8]) {
      const date2 = addSecs(date, secs)
      // lors de la création d'une date la partie décimale du timestamp en ms est virée (pas d'arrondi)
      expect(date2.getTime()).to.equals(Math.floor(ts + secs * 1000), `Pb avec ${secs}s qui donne ${date2}`)
    }
  })
  it('plante si on lui file une date invalide ou un nb de jours invalide (NaN ou infini)', () => {
    for (const badDate of badDates) {
      const fn = addSecs.bind(null, badDate, 1)
      expect(fn).to.throw(TypeError, 'date invalide', `pb avec ${badDate}`)
    }
    for (const badNumber of badNumbers) {
      const fn = addSecs.bind(null, date, badNumber)
      expect(fn).to.throw(TypeError, 'invalide', `pb avec ${badNumber}`)
    }
  })
  it('plante si on lui file nb de secondes trop grand', () => {
    for (const tooBig of [-1e15, 1e15]) {
      const fn = addSecs.bind(null, date, tooBig)
      expect(fn).to.throw(RangeError, 'offset trop grand', `pb avec ${tooBig}`)
    }
  })
})

describe('diffDays', () => {
  it('retourne une différence en nb de jours, arrondie à l’entier le plus proche', () => {
    for (const [name, days] of Object.entries(offsetDays)) {
      const date2 = dates[name]
      expect(diffDays(date, date2)).to.equals(days, `pb entre ${date} et ${date2}`)
      expect(diffDays(date2, date)).to.equals(-days, `pb entre ${date2} et ${date}`)
    }
  })
  it('retourne une différence en nb de jours, arrondie à l’entier le plus proche', () => {
    for (const [name, days] of Object.entries(offsetDaysFloat)) {
      const date2 = dates[name]
      expect(diffDays(date, date2, { isFloat: true })).to.equals(days, `pb entre ${date} et ${date2}`)
      expect(diffDays(date2, date, { isFloat: true })).to.equals(-days, `pb entre ${date2} et ${date}`)
    }
  })
  it('plante avec une date invalide', () => {
    for (const badDate of badDates) {
      const fn = diffDays.bind(null, date, badDate)
      const fn2 = diffDays.bind(null, badDate, date)
      expect(fn).to.throw(TypeError, 'date invalide', `pb avec ${badDate}`)
      expect(fn2).to.throw(TypeError, 'date invalide', `pb avec ${badDate}`)
    }
  })
})

describe('diffSecs', () => {
  it('retourne une différence en nb de secondes, arrondie à l’entier le plus proche', () => {
    for (const [name, sec] of Object.entries(offsetS)) {
      const date2 = dates[name]
      expect(diffSecs(date, date2)).to.equals(sec, `pb entre ${date} et ${date2}`)
      expect(diffSecs(date2, date)).to.equals(-sec, `pb entre ${date2} et ${date}`)
    }
  })
  it('plante avec une date invalide', () => {
    for (const badDate of badDates) {
      const fn = diffSecs.bind(null, date, badDate)
      const fn2 = diffSecs.bind(null, badDate, date)
      expect(fn).to.throw(TypeError, 'date invalide', `pb avec ${badDate}`)
      expect(fn2).to.throw(TypeError, 'date invalide', `pb avec ${badDate}`)
    }
  })
})

describe('endOfDay', () => {
  it('retourne la date de la dernière ms du jour', () => {
    const date = new Date('2023-01-02')
    const endOfDate = endOfDay(date)
    expect(endOfDate).to.be.a('Date')
    expect(formatDate({ date: endOfDate })).to.equals(formatDate({ date }))
    expect(formatTime({ date: endOfDate })).to.equals('23:59:59.999')
    const endOfToday = endOfDay()
    expect(endOfToday).to.be.a('Date')
    expect(formatDate({ date: endOfToday })).to.equals(formatDate())
    expect(formatTime({ date: endOfToday })).to.equals('23:59:59.999')
  })
})

describe('isOrdered', () => {
  it('retourne le booléen attendu', () => {
    const date1 = new Date('1002-02-02')
    const date2 = new Date('2002-02-03')
    const date2b = new Date('2002-02-03')
    const date3 = new Date('3042-02-04')
    const date4 = new Date(date3.getTime() + 1) // 1ms plus tard
    // dates ≠
    for (const [d1, d2] of [
      [date1, date2],
      [date1, date3],
      [date2, date3],
      [date3, date4]
    ]) {
      expect(isOrdered(d1, d2)).to.be.true
      expect(isOrdered(d1, d2, { strictMode: true })).to.be.true
      expect(isOrdered(d2, d1)).to.be.false
      expect(isOrdered(d2, d1, { strictMode: true })).to.be.false
    }
    // dates égales
    for (const [d1, d2] of [
      [date2, date2],
      [date2, date2b],
      [date2b, date2]
    ]) {
      expect(isOrdered(d1, d2)).to.be.true
      expect(isOrdered(d1, d2, { strictMode: true })).to.be.false
      expect(isOrdered(d2, d1)).to.be.true
      expect(isOrdered(d2, d1, { strictMode: true })).to.be.false
    }
  })
  it('plante avec une date invalide', () => {
    const today = new Date()
    for (const badDate of badDates) {
      const fn = isOrdered.bind(null, today, badDate)
      const fn2 = isOrdered.bind(null, badDate, today)
      expect(fn).to.throw(TypeError, 'date invalide', `pb avec ${badDate}`)
      expect(fn2).to.throw(TypeError, 'date invalide', `pb avec ${badDate}`)
    }
  })
})

describe('startOfDay', () => {
  it('retourne la date de la dernière ms du jour', () => {
    const date = new Date('2023-01-02')
    const startOfDate = startOfDay(date)
    expect(startOfDate).to.be.a('Date')
    expect(formatDate({ date: startOfDate })).to.equals(formatDate({ date }))
    expect(formatTime({ date: startOfDate })).to.equals('00:00:00.000')
    const startOfToday = startOfDay()
    expect(startOfToday).to.be.a('Date')
    expect(formatDate({ date: startOfToday })).to.equals(formatDate())
    expect(formatTime({ date: startOfToday })).to.equals('00:00:00.000')
  })
})
