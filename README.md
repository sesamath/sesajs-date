# sesajs-date

Un module pour manipuler des dates, pour remplacer moment. 

Prévu au départ pour donner une interface qui masque la lib sous-jacente utilisée (pour pouvoir en changer si besoin), 
et finalement y'a pas eu besoin de lib sous-jacente vu nos besoins basiques.

C'est très rudimentaire, ça ne gère que 3 formats de date
* dd/MM/YYYY
* YYYY-MM-dd
* json : YYYY-MM-ddThh:mm:ss.SSSZ (sans les ms possible aussi, années négatives avec -YYYYYY-MM-ddThh:mm:ss.SSSZ)

Les conversions string => date
* parseDateFr
* parseDateIso
* parseDateJson
* parse (pour les 3 formats)
et date => string
* formatDate (pour date seulement)
* formatDateTime (pour date + heure)
* formatTime (pour heure seulement)

Avec quelques opérations
* addDays : pour ajouter des jours à une date
* addSecs : pour ajouter des secondes à une date
* diffDays : pour calculer une différence en jours (arrondi) entre deux dates
* diffSecs : pour calculer une différence en secondes (arrondi) entre deux dates

Mais ça suffit à répondre à nos besoins

## usage
```shell
# syntaxe esModule
import { addDays, diffDays, formatDate } from 'sesajs-date'
# ou commonJs
const { addDays, diffDays, formatDate } = require('sesajs-date')
# à priori inutile d'aller chercher chaque source avec la syntaxe esModule, le tree shaking s'occupe de ne charger que ce qui est nécessaire
# mais en cjs on peut n'inclure qu'une partie si besoin
const { formatDate } = require('sesajs-date/cjs/format')
const { addDays, diffDays, formatDate } = require('sesajs-date/cjs/math')
```

# documentation
Un IDE devrait afficher la doc de chaque fonction lors de son usage, sinon il est possible de la générer en html avec

```
# si cela n'a pas encore été fait, installer les dépendances
npm i
# puis générer la doc
npm run doc
``` 
puis aller voir documentation/index.html 

# bugs
Les tests sont sensés tout couvrir, mais s'il restait un bug merci de le signaler sur
https://forge.apps.education.fr/sesamath/sesajs-date/-/issues
