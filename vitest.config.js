// cf https://vitest.dev/config/
export default {
  test: {
    include: ['test/**/*.test.js'],
    reporter: 'verbose'
  }
}
