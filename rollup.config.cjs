const fs = require('fs')
const path = require('path')

const config = []
const srcDir = path.resolve(__dirname, 'source')
fs.readdirSync(srcDir).forEach(function (file) {
  if (/\.js$/.test(file)) {
    config.push({
      input: `source/${file}`,
      output: {
        file: `cjs/${file}`,
        format: 'cjs'
      }
    })
  }
})

module.exports = config
